# IPcalvel

IPcalvel is a simple one page laravel project that only calculates information about a subnet from any ip/mask combination given in CIDR notation.

## Requirements


The following PHP extensions are required:

- [GNU Multiple Precision PHP Extension](https://www.php.net/manual/en/book.gmp.php)

and by virtue of Laravel:

- PHP >= 7.3
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

as well as `node.js` and `npm`


## Installation

Install all required dependencies first.


```
composer update
cp .env.example .env
php artisan key:generate
npm install && npm run dev
```

## Usage

Simply run 

```
php artisan serve
``` 
and navigate to `http://127.0.0.1:8000/` - or set it up with your favourite HTTP server



## Example Output
Input: 
```
10.0.0.0/10
```
Output: 
```
Basic Information
    Network: 10.0.0.0
    First Usable: 10.0.0.1
    Last Usable: 10.63.255.254
    Hosts Available: 4194302
```
