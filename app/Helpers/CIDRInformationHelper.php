<?php

namespace App\Helpers;

use \Exception;

/**
 * CIDRInformationHelper - for calculating information for a given subnet.
 *
 * Given a valid IPv4 or IPv6 address, this class will calculate the usable range and number of hosts.
 * For a valid IPv4 address this class will additionally calculate the subnet mask in quads, the network and the
 * broadcast address if applicable.
 *
 * Example Usage:
 * $ip = new CIDRInformationHelper("192.168.0.0", 24);
 * echo "This subnet has " . $ip->AddressableHosts . " addressable hosts.\n";
 * echo "The broadcast address is " . $ip->IPV4Only->Broadcast;
 *
 *
 * @package CIDRInformationHelper
 * @author Lawrence Cook <lawrencescook@gmail.com>
 * @version 0
 */
class CIDRInformationHelper
{
    //
    // Public Variables
    //

    /** @var string The given IP address */
    public $Address;

    /** @var int IP version */
    public $Version;

    /** @var int Length of mask in bits */
    public $MaskBits;

    /** @var string Subnet mask as an address */
    public $MaskAsAddress;

    /** @var string Number of hosts in range */
    public $Hosts;

    /** @var string Number of addressable hosts in range */
    public $AddressableHosts;

    /** @var string Start address of the IP range */
    public $First;

    /** @var string End address of the IP range */
    public $Last;

    /** @var string First usable host address in IP range */
    public $FirstUsable;

    /** @var string Last usable host address in IP range */
    public $LastUsable;

    /** @var string Network address of IP range */
    public $Network;

    /** @var string Broadcast address of IP range (IPv4 only) */
    public $Broadcast;


    //
    // Private Variables
    //

    /** @var string IP address represented as a long */
    private $AddressAsLong;

    /** @var string Mask represented as a long */
    private $MaskAsLong;

    /** @var string Start of IP range as a long */
    private $FirstAsLong;

    /** @var string End of IP range as a long */
    private $LastAsLong;

    /** @var string First usable IP in range as a long */
    private $FirstUsableAsLong;

    /** @var string Last usable IP in range as a long */
    private $LastUsableAsLong;

    /** @var int Bit length of address */
    private $_bits;

    // Our private constants
    private const MAX_MASK_IPV4 = 32;
    private const MAX_MASK_IPV6 = 128;

    // Error Codes:
    public const ERROR_NO_MASK = 1;
    public const ERROR_INVALID_IP = 2;


    /**
     * CIDRInformationHelper constructor.
     * @param bool $ipAddr
     * @param bool $CIDRSuffix
     * @throws Exception
     */
    function __construct($ipAddr = false, $CIDRSuffix = false)
    {
        // removing this or removing exceptions from calculate would make this file completely PSR-1 compliant
        // but I don't want to.
        $this->calculate($ipAddr, $CIDRSuffix);
    }


    /**
     * Calculate & populate all the public member variables in class. Calculates first address, last address, first usable
     * address, last usable address, length of IP range and the number of addressable hosts.
     *
     * @param $ipAddr IP address
     * @param $CIDRSuffix CIDR suffix, number of bits in mask
     * @return bool
     * @throws Exception
     */
    function calculate($ipAddr, $CIDRSuffix){
        // If we have been given only one argument, let's get the mask.
        if(strpos($ipAddr, '/') !== false){
            $cidr = explode("/", $ipAddr);
            $ipAddr = $cidr[0];
            $CIDRSuffix = $cidr[1];
        }

        // if $CIDRSuffix is not supplied or 0 then we can't continue
        if(!$CIDRSuffix || $CIDRSuffix <= 0) throw new Exception('Invalid mask', self::ERROR_NO_MASK);

        $CIDRSuffix = (int)$CIDRSuffix;

        // Check if its a valid IP and populate $Version, record length
        if($this->isIPv4($ipAddr) && $CIDRSuffix <= self::MAX_MASK_IPV4) {
            $this->Version = 4;
            $this->_bits = self::MAX_MASK_IPV4;
        }

        if($this->isIPv6($ipAddr) && $CIDRSuffix <= self::MAX_MASK_IPV6) {
            $this->Version = 6;
            $this->_bits = self::MAX_MASK_IPV6;
        }

        // If there's no version detected then we can't continue
        if(empty($this->Version)) throw new Exception('Invalid IP address', self::ERROR_INVALID_IP);

        // We made it here, everything is valid.

        // Get address and mask info from IP
        $this->Address = $ipAddr;
        $this->AddressAsLong = $this->ip2long($this->Address, $this->Version);
        $this->MaskBits = (int)$CIDRSuffix;

        $this->MaskAsLong = $this->bitshiftLeft(-1, $this->_bits - $this->MaskBits);

        // Work out number of available hosts.
        $this->Hosts = $this->bitshiftLeft(1, $this->_bits - $this->MaskBits);

        // Get first and last address in range, do not trust that the provided address is the first address
        $this->FirstAsLong = $this->bitwiseAnd($this->AddressAsLong, $this->MaskAsLong);

        $this->LastAsLong = $this->Add(
            $this->FirstAsLong,
            gmp_pow(
                2,
                ($this->_bits - $this->MaskBits)
            ) - 1
        );

        $this->Network = $this->long2ip($this->FirstAsLong, $this->Version);

        if($this->Version === 4 && $this->MaskBits <= 30){
            //
            // IPv4 has a broadcast address, where IPv6 does not.
            // I also stuffed some hacks in here to deal with /32 & /31
            //
            $this->Broadcast = $this->long2ip($this->LastAsLong, $this->Version);

            $this->FirstUsableAsLong = $this->add($this->FirstAsLong, 1);
            $this->LastUsableAsLong = $this->sub($this->LastAsLong, 1);
            $this->AddressableHosts = $this->sub($this->Hosts, 2);
        }else{
            //
            // IPv6 has no broadcast or network addresses, the entire space is usable.
            //
            $this->FirstUsableAsLong = $this->FirstAsLong;
            $this->LastUsableAsLong = $this->LastAsLong;
            $this->AddressableHosts = $this->Hosts;
        }

        // Populate the human readable variables
        $this->FirstUsable =  $this->long2ip($this->FirstUsableAsLong, $this->Version);
        $this->LastUsable =  $this->long2ip($this->LastUsableAsLong, $this->Version);
        $this->First = $this->long2ip($this->FirstAsLong, $this->Version);
        $this->Last = $this->long2ip($this->LastAsLong, $this->Version);
        $this->MaskAsAddress = $this->long2ip($this->MaskAsLong, $this->Version);

        return true;
    }


    /**
     * Check if a given string is an IPv4 address. Returns true only if the IP is valid.
     *
     * @param string $ipAddr
     * @return bool
     */
    public function isIPv4($ipAddr){
        return !!filter_var($ipAddr, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }


    /**
     * Check if a given string is an IPv6 address. Returns true only if the IP is valid.
     *
     * @param string $ipAddr
     * @return bool
     */
    public function isIPv6($ipAddr){
        return !!filter_var($ipAddr, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
    }


    /**
     * Convert a human readable IP address to a long, with IPv4/IPv6 support
     *
     * @param string $ipAddr
     * @param int $version
     * @return string IP address represented as a long
     */
    public function ip2long($ipAddr, $version){
        switch($version){
            case 4:
                return ip2long($ipAddr);
            case 6:
                return $this->ip2long6($ipAddr);
        }
        return false;
    }


    /**
     * Convert a long to a human readable IP address, with IPv4/IPv6 support
     *
     * @param string $long
     * @param int $version
     * @return string IP represented as a human readable string
     */
    public function long2ip($long, $version){
        switch($version){
            case 4:
                return long2ip($long);
            case 6:
                return $this->long2ip6($long);
        }
        return false;
    }


    /**
     * Convert an IPv6 address to a string encoded long
     * Borrowed from: https://stackoverflow.com/a/37250452
     *
     * @param string $ipAddr
     * @return string IP address represented as a long
     */
    public function ip2long6($ipAddr) {
        // Number too big, have to rely on php extensions :D
        if (!function_exists('gmp_init'))
            trigger_error('GMP extension not installed!', E_USER_ERROR);

        return (string) gmp_import(inet_pton($ipAddr));
    }


    /**
     * Convert a string encoded long to an IPv6 address
     * Borrowed from: https://stackoverflow.com/a/37250452
     *
     * @param string $longIP
     * @return string IP represented as a human readable string
     */
    public function long2ip6($longIP) {
        if (!function_exists('gmp_init'))
            trigger_error('GMP extension not installed!', E_USER_ERROR);

        return inet_ntop(str_pad(gmp_export($longIP), 16, "\0", STR_PAD_LEFT));
    }


    /**
     * Perform a logical AND on two given numbers
     *
     * @param string $a
     * @param string $b
     * @return string
     */
    function bitwiseAnd($a, $b){
        return gmp_strval(gmp_and($a, $b));
    }

    /*
     * Square a given number by an exponent
     *
     * @param string $x
     * @param string $exponent
     * @return string
     */
    function power($x, $exponent){
        return gmp_strval(gmp_pow($x,$exponent));
    }


    /**
     * Add two given numbers
     *
     * @param string $a
     * @param string $b
     * @return string
     */
    function add($a, $b){
        return gmp_strval(gmp_add($a,$b));
    }


    /**
     * Subtract two given numbers
     *
     * @param string $a
     * @param string $b
     * @return string
     */
    function sub($a, $b){
        return gmp_strval(gmp_sub($a,$b));
    }


    /**
     * Invert a given number by another number
     *
     * @param string $x
     * @param string $y
     * @return bool
     */
    function invert($x, $y){
        return gmp_strval(gmp_invert($x, $y));
    }


    /**
     * Bitshift a given number to the left
     *
     * @param string $x Number to bitshift
     * @param string $n Number of bits to shift by
     * @return string Result encoded in a string
     */
    public function bitshiftLeft($x, $n){
        return gmp_strval(gmp_mul($x, gmp_pow(2, $n)));
    }
}