<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\CIDRInformationHelper;

class SubnetCalculatorController extends Controller
{
    public function index(Request $request){
        return view('subnet');
    }

    public function calculate(Request $request){
        //TODO: create complex validation rules, laravel supports this
        // If it's a valid IPv4 address, max mask should be 32
        // If it's a valid IPv6 address, max mask should be 128

        $this->validate($request, [
            'ip' => 'required|ip',
            'mask' => 'required|max:128',
        ]);

        $ip = $request->input('ip');
        $mask = $request->input('mask');

        $response = [];

        try{
            $CIDRObj = new CIDRInformationHelper($ip, $mask);
            $response['success'] = true;
            $response['payload'] = $CIDRObj;
        }catch(\Exception $e){
            $response['success'] = false;
            $response['message'] = $e->getMessage();
            return response()->json($response, 422);
        }

        return response()->json($response);
    }
}
