<?php


namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers\CIDRInformationHelper;

class CIDRInformationHelperTest extends TestCase
{

    // this could all be expanded, but did not have enough time to do so. CIDRInformationHelper would need adapting
    // to throw errors more consciously

    public function testValidIpv4(){
        $CIDRInformationHelper = new CIDRInformationHelper("10.0.0.0", 24);
        $this->assertTrue($CIDRInformationHelper->isIPv4("10.0.0.0"));
    }

    public function testInvalidIpv4(){
        $CIDRInformationHelper = new CIDRInformationHelper("10.0.0.0", 24);
        $this->assertFalse($CIDRInformationHelper->isIPv4("10.0.0"));
    }

    public function testValidIpv6(){
        $CIDRInformationHelper = new CIDRInformationHelper("fe80::a00:27ff:fe80:25c3", 24);
        $this->assertTrue($CIDRInformationHelper->isIPv6("fe80::a00:27ff:fe80:25c3"));
    }

    public function testInvalidIpv6(){
        $CIDRInformationHelper = new CIDRInformationHelper("fe80::a00:27ff:fe80:25c3", 24);
        $this->assertFalse($CIDRInformationHelper->isIPv6("ge80::a00:27ff:fe80:25c3"));
    }
}