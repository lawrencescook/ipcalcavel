@extends('master')
@section('title', 'Subnet Calculator')
@section('heading', 'Subnet Calculator')

@section('content')
<script>
    $( document ).ready(function() {
        $( "#calculate" ).click(function() {
            let cidr = $('#cidrNotation').val();
            cidr = cidr.split("/",2);

            function showError(message){
                $('#error_message').html(message);
                $('#error_message').show();
                $('#result_section').hide();
            }

            function showResult(){
                $('#error_message').hide();
                $('#result_section').show();
            }

            $.getJSON("api/SubnetCalculator/calculate", {ip: cidr[0], mask: cidr[1]}, function( data ) {
                if(data.success) {
                    $.each(data.payload, function (key, value) {
                        $('[name=' + key + ']', ".results").html(value);
                    });
                    showResult();
                }
            }).fail(function(data) {
                if (data.status == 422){
                    showError(data.responseJSON.message);
                    console.log(data);
                }else{
                    showError("Unknown Server Error");
                }
            });

            return false;
        });
    });
</script>
<p>Calculate information about a given subnet</p>
<div class="card p-3 mb-2 bg-secondary text-white">
    <div class="card-body">
        <form class="form">
            <div class="form-group">
                <label for="cidrNotation">CIDR</label>
                <input type="text" class="form-control" id="cidrNotation" placeholder="10.0.0.0/32">
            </div>
            <button id="calculate" type="submit" class="btn btn-primary float-right">Calculate</button>
        </form>
    </div>
</div>
<hr />
<div id="error_message" class="alert alert-danger" style="display: none">
</div>
<div id="result_section" style="display: none" class="p-3 mb-2 bg-dark text-white">
    <h2>Basic Information</h2>
    <div class="results">
        <div class="row">
            <div class="col-md-6">Network:</div>
            <div class="col-md-6" name="Network"></div>
        </div>
        <div class="row">
            <div class="col-md-6">First Usable:</div>
            <div class="col-md-6" name="FirstUsable"></div>
        </div>
        <div class="row">
            <div class="col-md-6">Last Usable:</div>
            <div class="col-md-6" name="LastUsable"></div>
        </div>
        <div class="row">
            <div class="col-md-6">Hosts Available:</div>
            <div class="col-md-6" name="AddressableHosts"></div>
        </div>
    </div>
    <hr />
    <h2>Detailed Information</h2>
    <div class="results">
        <div class="row">
            <div class="col-md-6">Start:</div>
            <div class="col-md-6" name="First"></div>
        </div>
        <div class="row">
            <div class="col-md-6">End:</div>
            <div class="col-md-6" name="Last"></div>
        </div>
        <div class="row">
            <div class="col-md-6">First Usable:</div>
            <div class="col-md-6" name="FirstUsable"></div>
        </div>
        <div class="row">
            <div class="col-md-6">Last Usable:</div>
            <div class="col-md-6" name="LastUsable"></div>
        </div>
        <div class="row">
            <div class="col-md-6">Subnet Mask:</div>
            <div class="col-md-6" name="MaskAsAddress"></div>
        </div>
        <div class="row">
            <div class="col-md-6">Network:</div>
            <div class="col-md-6" name="Network"></div>
        </div>
        <div class="row">
            <div class="col-md-6">Broadcast:</div>
            <div class="col-md-6" name="Broadcast"></div>
        </div>
        <div class="row">
            <div class="col-md-6">Hosts:</div>
            <div class="col-md-6" name="Hosts"></div>
        </div>
        <div class="row">
            <div class="col-md-6">Addressable Hosts:</div>
            <div class="col-md-6" name="AddressableHosts"></div>
        </div>
    </div>
</div>
@stop