<!doctype html>
<html lang="en">
    <head>
        <title>IPcalvel | @yield('title')</title>
        <link href="/css/app.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"
                integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
                crossorigin="anonymous"></script>
    </head>
    <body class="p-3 mb-2 bg-dark text-white">
        <div class="container" >
            <h1>@yield('heading')</h1>

            @yield('content')
        </div>
    </body>
</html>